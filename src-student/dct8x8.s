	.file	"dct8x8.c"
	.text
	.p2align 4,,15
	.globl	_Z17slow_float_dct8x8PA8_sS0_
	.type	_Z17slow_float_dct8x8PA8_sS0_, @function
_Z17slow_float_dct8x8PA8_sS0_:
.LFB26:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	mcount
	movsd	.LC1(%rip), %xmm5
	leaq	-176(%rbp), %rax
	movq	%rdi, -280(%rbp)
	movq	%rsi, -312(%rbp)
	movsd	%xmm5, -200(%rbp)
	movq	$Av, -296(%rbp)
	movl	$0, -288(%rbp)
	movq	%rax, -304(%rbp)
	movl	$0, -284(%rbp)
	.p2align 4,,10
	.p2align 3
.L2:
	movl	-284(%rbp), %r15d
	leal	(%r15,%r15,2), %ebx
	leal	0(,%r15,8), %r12d
	cvtsi2sd	%ebx, %xmm0
	mulsd	.LC2(%rip), %xmm0
	mulsd	.LC3(%rip), %xmm0
	call	cos
	leal	(%r15,%r15,4), %eax
	movsd	%xmm0, -216(%rbp)
	cvtsi2sd	%eax, %xmm0
	mulsd	.LC2(%rip), %xmm0
	mulsd	.LC3(%rip), %xmm0
	call	cos
	movl	%r12d, %edi
	movsd	%xmm0, -248(%rbp)
	addl	%r15d, %r12d
	subl	%r15d, %edi
	cvtsi2sd	%edi, %xmm0
	mulsd	.LC2(%rip), %xmm0
	mulsd	.LC3(%rip), %xmm0
	call	cos
	movsd	%xmm0, -240(%rbp)
	cvtsi2sd	%r12d, %xmm0
	xorl	%r12d, %r12d
	mulsd	.LC2(%rip), %xmm0
	mulsd	.LC3(%rip), %xmm0
	call	cos
	movsd	%xmm0, -232(%rbp)
	cvtsi2sd	-288(%rbp), %xmm0
	mulsd	.LC2(%rip), %xmm0
	mulsd	.LC3(%rip), %xmm0
	call	cos
	leal	(%r15,%rbx,4), %eax
	movsd	%xmm0, -224(%rbp)
	cvtsi2sd	%eax, %xmm0
	mulsd	.LC2(%rip), %xmm0
	mulsd	.LC3(%rip), %xmm0
	call	cos
	movl	%r15d, %eax
	movsd	%xmm0, -208(%rbp)
	sall	$4, %eax
	subl	%r15d, %eax
	cvtsi2sd	%eax, %xmm0
	mulsd	.LC2(%rip), %xmm0
	mulsd	.LC3(%rip), %xmm0
	call	cos
	movq	-304(%rbp), %rax
	movsd	%xmm0, -256(%rbp)
	movsd	.LC1(%rip), %xmm0
	movq	%rax, -264(%rbp)
	movq	-296(%rbp), %rax
	movq	%rax, -272(%rbp)
	.p2align 4,,10
	.p2align 3
.L7:
	movss	(%rax), %xmm4
	leal	(%r12,%r12), %ebx
	movq	-280(%rbp), %r14
	xorl	%r15d, %r15d
	xorpd	%xmm1, %xmm1
	cvtps2pd	%xmm4, %xmm4
	leal	(%rbx,%r12), %r13d
	movsd	%xmm4, -192(%rbp)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L3:
	cvtsi2sd	%r13d, %xmm0
	addl	%ebx, %r13d
	movsd	%xmm1, -184(%rbp)
	mulsd	.LC2(%rip), %xmm0
	mulsd	.LC3(%rip), %xmm0
	call	cos
	movsd	-184(%rbp), %xmm1
.L5:
	movswl	(%r14), %esi
	mulsd	-192(%rbp), %xmm0
	addl	$1, %r15d
	movsd	-200(%rbp), %xmm2
	addq	$16, %r14
	cvtsi2sd	%esi, %xmm3
	movswl	-14(%r14), %esi
	mulsd	%xmm0, %xmm2
	mulsd	%xmm3, %xmm2
	cvtsi2sd	%esi, %xmm3
	movswl	-12(%r14), %esi
	addsd	%xmm1, %xmm2
	movsd	-216(%rbp), %xmm1
	mulsd	%xmm0, %xmm1
	mulsd	%xmm3, %xmm1
	cvtsi2sd	%esi, %xmm3
	movswl	-10(%r14), %esi
	addsd	%xmm1, %xmm2
	movsd	-248(%rbp), %xmm1
	mulsd	%xmm0, %xmm1
	mulsd	%xmm3, %xmm1
	cvtsi2sd	%esi, %xmm3
	movswl	-8(%r14), %esi
	addsd	%xmm2, %xmm1
	movsd	-240(%rbp), %xmm2
	mulsd	%xmm0, %xmm2
	mulsd	%xmm3, %xmm2
	cvtsi2sd	%esi, %xmm3
	movswl	-6(%r14), %esi
	addsd	%xmm1, %xmm2
	movsd	-232(%rbp), %xmm1
	mulsd	%xmm0, %xmm1
	mulsd	%xmm3, %xmm1
	cvtsi2sd	%esi, %xmm3
	movswl	-4(%r14), %esi
	addsd	%xmm2, %xmm1
	movsd	-224(%rbp), %xmm2
	mulsd	%xmm0, %xmm2
	mulsd	%xmm3, %xmm2
	cvtsi2sd	%esi, %xmm3
	movswl	-2(%r14), %esi
	cmpl	$8, %r15d
	addsd	%xmm1, %xmm2
	movsd	-208(%rbp), %xmm1
	mulsd	%xmm0, %xmm1
	mulsd	-256(%rbp), %xmm0
	mulsd	%xmm3, %xmm1
	addsd	%xmm2, %xmm1
	cvtsi2sd	%esi, %xmm2
	mulsd	%xmm2, %xmm0
	addsd	%xmm0, %xmm1
	jne	.L3
	cvttsd2si	%xmm1, %eax
	movq	-264(%rbp), %rdx
	addl	$1, %r12d
	addq	$32, -272(%rbp)
	addq	$16, -264(%rbp)
	cmpl	$8, %r12d
	movw	%ax, (%rdx)
	je	.L4
	cvtsi2sd	%r12d, %xmm0
	mulsd	.LC2(%rip), %xmm0
	mulsd	.LC3(%rip), %xmm0
	call	cos
	movq	-272(%rbp), %rax
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L4:
	addl	$1, -284(%rbp)
	addl	$11, -288(%rbp)
	addq	$4, -296(%rbp)
	addq	$2, -304(%rbp)
	cmpl	$8, -284(%rbp)
	je	.L6
	cvtsi2sd	-284(%rbp), %xmm0
	mulsd	.LC2(%rip), %xmm0
	mulsd	.LC3(%rip), %xmm0
	call	cos
	movsd	%xmm0, -200(%rbp)
	jmp	.L2
.L6:
	movq	-176(%rbp), %rax
	movq	-312(%rbp), %rcx
	movq	%rax, (%rcx)
	movq	-168(%rbp), %rax
	movq	%rax, 8(%rcx)
	movq	-160(%rbp), %rax
	movq	%rax, 16(%rcx)
	movq	-152(%rbp), %rax
	movq	%rax, 24(%rcx)
	movq	-144(%rbp), %rax
	movq	%rax, 32(%rcx)
	movq	-136(%rbp), %rax
	movq	%rax, 40(%rcx)
	movq	-128(%rbp), %rax
	movq	%rax, 48(%rcx)
	movq	-120(%rbp), %rax
	movq	%rax, 56(%rcx)
	movq	-112(%rbp), %rax
	movq	%rax, 64(%rcx)
	movq	-104(%rbp), %rax
	movq	%rax, 72(%rcx)
	movq	-96(%rbp), %rax
	movq	%rax, 80(%rcx)
	movq	-88(%rbp), %rax
	movq	%rax, 88(%rcx)
	movq	-80(%rbp), %rax
	movq	%rax, 96(%rcx)
	movq	-72(%rbp), %rax
	movq	%rax, 104(%rcx)
	movq	-64(%rbp), %rax
	movq	%rax, 112(%rcx)
	movq	-56(%rbp), %rax
	movq	%rax, 120(%rcx)
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26:
	.size	_Z17slow_float_dct8x8PA8_sS0_, .-_Z17slow_float_dct8x8PA8_sS0_
	.p2align 4,,15
	.globl	_Z15slow_float_dct8PfS_
	.type	_Z15slow_float_dct8PfS_, @function
_Z15slow_float_dct8PfS_:
.LFB29:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	mcount
	xorl	%r15d, %r15d
	movl	$1, %r13d
	xorl	%ebx, %ebx
	movq	%rdi, %r12
	movq	%rsi, %r14
	movsd	.LC1(%rip), %xmm0
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L12:
	cvtsi2sd	%r13d, %xmm0
	addl	$1, %ebx
	addl	$1, %r13d
	addq	$4, %r14
	addl	$11, %r15d
	mulsd	.LC2(%rip), %xmm0
	mulsd	.LC3(%rip), %xmm0
	call	cos
.L10:
	movss	(%r12), %xmm1
	testl	%ebx, %ebx
	cvtps2pd	%xmm1, %xmm1
	mulsd	%xmm0, %xmm1
	unpcklpd	%xmm1, %xmm1
	cvtpd2ps	%xmm1, %xmm1
	je	.L14
	leal	(%rbx,%rbx,2), %eax
	xorps	%xmm3, %xmm3
	cvtsi2sd	%eax, %xmm0
	movl	%eax, -60(%rbp)
	addss	%xmm3, %xmm1
	movss	%xmm1, -52(%rbp)
	mulsd	.LC2(%rip), %xmm0
	mulsd	.LC3(%rip), %xmm0
	call	cos
	movss	4(%r12), %xmm1
	leal	(%rbx,%rbx,4), %edx
	cvtps2pd	%xmm1, %xmm1
	mulsd	%xmm0, %xmm1
	cvtsi2sd	%edx, %xmm0
	unpcklpd	%xmm1, %xmm1
	cvtpd2ps	%xmm1, %xmm1
	mulsd	.LC2(%rip), %xmm0
	addss	-52(%rbp), %xmm1
	mulsd	.LC3(%rip), %xmm0
	movss	%xmm1, -52(%rbp)
	call	cos
	movss	8(%r12), %xmm2
	leal	0(,%rbx,8), %edx
	movss	-52(%rbp), %xmm1
	cvtps2pd	%xmm2, %xmm2
	movl	%edx, %ecx
	movl	%edx, -56(%rbp)
	subl	%ebx, %ecx
	mulsd	%xmm0, %xmm2
	cvtsi2sd	%ecx, %xmm0
	unpcklpd	%xmm2, %xmm2
	cvtpd2ps	%xmm2, %xmm2
	mulsd	.LC2(%rip), %xmm0
	addss	%xmm2, %xmm1
	mulsd	.LC3(%rip), %xmm0
	movss	%xmm1, -52(%rbp)
	call	cos
	movss	12(%r12), %xmm2
	movl	-56(%rbp), %edx
	movss	-52(%rbp), %xmm1
	cvtps2pd	%xmm2, %xmm2
	addl	%ebx, %edx
	mulsd	%xmm0, %xmm2
	cvtsi2sd	%edx, %xmm0
	unpcklpd	%xmm2, %xmm2
	cvtpd2ps	%xmm2, %xmm2
	mulsd	.LC2(%rip), %xmm0
	addss	%xmm1, %xmm2
	mulsd	.LC3(%rip), %xmm0
	movss	%xmm2, -52(%rbp)
	call	cos
	movss	16(%r12), %xmm1
	movss	-52(%rbp), %xmm2
	cvtps2pd	%xmm1, %xmm1
	mulsd	%xmm0, %xmm1
	cvtsi2sd	%r15d, %xmm0
	unpcklpd	%xmm1, %xmm1
	cvtpd2ps	%xmm1, %xmm1
	mulsd	.LC2(%rip), %xmm0
	addss	%xmm2, %xmm1
	mulsd	.LC3(%rip), %xmm0
	movss	%xmm1, -52(%rbp)
	call	cos
	movss	20(%r12), %xmm2
	movl	-60(%rbp), %eax
	movss	-52(%rbp), %xmm1
	cvtps2pd	%xmm2, %xmm2
	leal	(%rbx,%rax,4), %eax
	mulsd	%xmm0, %xmm2
	cvtsi2sd	%eax, %xmm0
	unpcklpd	%xmm2, %xmm2
	cvtpd2ps	%xmm2, %xmm2
	mulsd	.LC2(%rip), %xmm0
	addss	%xmm2, %xmm1
	mulsd	.LC3(%rip), %xmm0
	movss	%xmm1, -52(%rbp)
	call	cos
	movss	24(%r12), %xmm2
	movl	%ebx, %eax
	sall	$4, %eax
	movss	-52(%rbp), %xmm1
	cvtps2pd	%xmm2, %xmm2
	subl	%ebx, %eax
	mulsd	%xmm0, %xmm2
	cvtsi2sd	%eax, %xmm0
	unpcklpd	%xmm2, %xmm2
	cvtpd2ps	%xmm2, %xmm2
	mulsd	.LC2(%rip), %xmm0
	addss	%xmm2, %xmm1
	mulsd	.LC3(%rip), %xmm0
	movss	%xmm1, -52(%rbp)
	call	cos
	movss	28(%r12), %xmm2
	movss	-52(%rbp), %xmm1
	cmpl	$8, %r13d
	cvtps2pd	%xmm2, %xmm2
	mulsd	%xmm0, %xmm2
	unpcklpd	%xmm2, %xmm2
	cvtpd2ps	%xmm2, %xmm0
	addss	%xmm1, %xmm0
	mulss	.LC6(%rip), %xmm0
	movss	%xmm0, (%r14)
	jne	.L12
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	mulss	.LC4(%rip), %xmm1
	movss	.LC4(%rip), %xmm0
	xorps	%xmm4, %xmm4
	mulss	4(%r12), %xmm0
	movss	.LC4(%rip), %xmm2
	mulss	20(%r12), %xmm2
	addss	%xmm4, %xmm1
	addss	%xmm0, %xmm1
	movss	.LC4(%rip), %xmm0
	mulss	8(%r12), %xmm0
	addss	%xmm0, %xmm1
	movss	.LC4(%rip), %xmm0
	mulss	12(%r12), %xmm0
	addss	%xmm1, %xmm0
	movss	.LC4(%rip), %xmm1
	mulss	16(%r12), %xmm1
	addss	%xmm1, %xmm0
	movss	.LC4(%rip), %xmm1
	mulss	24(%r12), %xmm1
	addss	%xmm0, %xmm2
	movss	.LC4(%rip), %xmm0
	mulss	28(%r12), %xmm0
	addss	%xmm2, %xmm1
	addss	%xmm1, %xmm0
	mulss	.LC6(%rip), %xmm0
	movss	%xmm0, (%r14)
	jmp	.L12
	.cfi_endproc
.LFE29:
	.size	_Z15slow_float_dct8PfS_, .-_Z15slow_float_dct8PfS_
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC7:
	.string	"\n==== Input ===="
.LC8:
	.string	"input[%d] = %f\n"
.LC9:
	.string	"stage1[%d] = %f\n"
.LC11:
	.string	"\n==== Stage 2 ===="
.LC12:
	.string	"stage2[%d] = %f\n"
.LC17:
	.string	"\n==== Stage 3 ===="
.LC18:
	.string	"stage3[%d] = %f\n"
.LC25:
	.string	"\n==== Output ===="
.LC26:
	.string	"output[%d] = %f\n"
.LC28:
	.string	"\n==== Stage 1 ===="
	.text
	.p2align 4,,15
	.globl	_Z15fast_float_dct8PfS_
	.type	_Z15fast_float_dct8PfS_, @function
_Z15fast_float_dct8PfS_:
.LFB30:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	call	mcount
	xorl	%ebx, %ebx
	movq	%rdi, %r13
	movl	$.LC7, %edi
	movq	%rsi, %r12
	call	puts
	.p2align 4,,10
	.p2align 3
.L17:
	movss	0(%r13,%rbx,4), %xmm0
	movl	%ebx, %esi
	movl	$.LC8, %edi
	movl	$1, %eax
	addq	$1, %rbx
	cvtps2pd	%xmm0, %xmm0
	call	printf
	cmpq	$8, %rbx
	jne	.L17
	movss	0(%r13), %xmm0
	movl	$.LC28, %edi
	movss	28(%r13), %xmm1
	xorl	%ebx, %ebx
	addss	%xmm0, %xmm1
	movss	24(%r13), %xmm2
	movss	20(%r13), %xmm3
	subss	28(%r13), %xmm0
	movss	16(%r13), %xmm4
	movss	%xmm1, -96(%rbp)
	movss	4(%r13), %xmm1
	addss	%xmm1, %xmm2
	subss	24(%r13), %xmm1
	movss	%xmm0, -68(%rbp)
	movss	%xmm2, -92(%rbp)
	movss	8(%r13), %xmm2
	addss	%xmm2, %xmm3
	subss	20(%r13), %xmm2
	movss	%xmm1, -72(%rbp)
	movss	%xmm3, -88(%rbp)
	movss	12(%r13), %xmm3
	addss	%xmm3, %xmm4
	subss	16(%r13), %xmm3
	movss	%xmm2, -76(%rbp)
	movss	%xmm4, -84(%rbp)
	movss	%xmm3, -80(%rbp)
	call	puts
	.p2align 4,,10
	.p2align 3
.L19:
	movss	-96(%rbp,%rbx,4), %xmm0
	movl	%ebx, %esi
	movl	$.LC9, %edi
	movl	$1, %eax
	addq	$1, %rbx
	cvtps2pd	%xmm0, %xmm0
	call	printf
	cmpq	$8, %rbx
	jne	.L19
	movss	-84(%rbp), %xmm0
	movl	$.LC11, %edi
	movss	-96(%rbp), %xmm1
	xorb	%bl, %bl
	movss	-92(%rbp), %xmm3
	movaps	%xmm1, %xmm4
	movaps	%xmm3, %xmm7
	subss	%xmm0, %xmm1
	movss	-88(%rbp), %xmm2
	addss	%xmm2, %xmm7
	movss	-80(%rbp), %xmm5
	subss	%xmm2, %xmm3
	movss	-72(%rbp), %xmm2
	addss	%xmm0, %xmm4
	movss	-76(%rbp), %xmm0
	movss	%xmm1, -112(%rbp)
	movss	%xmm1, -52(%rbp)
	movaps	%xmm2, %xmm1
	movss	%xmm7, -104(%rbp)
	subss	%xmm0, %xmm1
	movss	%xmm4, -100(%rbp)
	addss	%xmm2, %xmm0
	movss	%xmm4, -64(%rbp)
	movss	%xmm7, -60(%rbp)
	movss	-68(%rbp), %xmm7
	unpcklps	%xmm1, %xmm1
	movss	%xmm3, -108(%rbp)
	unpcklps	%xmm0, %xmm0
	movss	%xmm3, -56(%rbp)
	movss	%xmm5, -116(%rbp)
	cvtps2pd	%xmm1, %xmm1
	cvtps2pd	%xmm0, %xmm0
	mulsd	.LC10(%rip), %xmm1
	movss	%xmm5, -48(%rbp)
	mulsd	.LC10(%rip), %xmm0
	movss	%xmm7, -128(%rbp)
	movss	%xmm7, -36(%rbp)
	unpcklpd	%xmm1, %xmm1
	unpcklpd	%xmm0, %xmm0
	cvtpd2ps	%xmm1, %xmm6
	cvtpd2ps	%xmm0, %xmm4
	movss	%xmm6, -120(%rbp)
	movss	%xmm6, -44(%rbp)
	movss	%xmm4, -124(%rbp)
	movss	%xmm4, -40(%rbp)
	call	puts
	.p2align 4,,10
	.p2align 3
.L21:
	movss	-64(%rbp,%rbx,4), %xmm0
	movl	%ebx, %esi
	movl	$.LC12, %edi
	movl	$1, %eax
	addq	$1, %rbx
	cvtps2pd	%xmm0, %xmm0
	call	printf
	cmpq	$8, %rbx
	jne	.L21
	movss	-100(%rbp), %xmm1
	movss	-104(%rbp), %xmm2
	movl	$.LC17, %edi
	movsd	.LC10(%rip), %xmm0
	movss	-112(%rbp), %xmm3
	cvtps2pd	%xmm1, %xmm1
	xorb	%bl, %bl
	cvtps2pd	%xmm2, %xmm2
	mulsd	.LC10(%rip), %xmm1
	cvtps2pd	%xmm3, %xmm3
	mulsd	%xmm2, %xmm0
	mulsd	.LC13(%rip), %xmm2
	movsd	.LC15(%rip), %xmm4
	addsd	%xmm1, %xmm0
	mulsd	%xmm3, %xmm4
	unpcklpd	%xmm0, %xmm0
	cvtpd2ps	%xmm0, %xmm5
	movapd	%xmm2, %xmm0
	movsd	.LC14(%rip), %xmm2
	addsd	%xmm1, %xmm0
	unpcklpd	%xmm0, %xmm0
	movss	%xmm5, -100(%rbp)
	movss	%xmm5, -96(%rbp)
	cvtpd2ps	%xmm0, %xmm6
	movss	-108(%rbp), %xmm0
	cvtps2pd	%xmm0, %xmm0
	movapd	%xmm0, %xmm1
	mulsd	.LC16(%rip), %xmm0
	movss	%xmm6, -104(%rbp)
	mulsd	%xmm2, %xmm1
	movss	%xmm6, -92(%rbp)
	mulsd	%xmm3, %xmm2
	movss	-116(%rbp), %xmm6
	movaps	%xmm6, %xmm5
	addsd	%xmm4, %xmm1
	addsd	%xmm2, %xmm0
	unpcklpd	%xmm1, %xmm1
	unpcklpd	%xmm0, %xmm0
	cvtpd2ps	%xmm1, %xmm7
	cvtpd2ps	%xmm0, %xmm4
	movss	%xmm7, -108(%rbp)
	movss	%xmm7, -88(%rbp)
	movss	%xmm4, -112(%rbp)
	movss	%xmm4, -84(%rbp)
	movss	-120(%rbp), %xmm4
	addss	%xmm4, %xmm5
	movss	%xmm5, -116(%rbp)
	movss	%xmm5, -80(%rbp)
	movaps	%xmm6, %xmm5
	subss	%xmm4, %xmm5
	movss	-128(%rbp), %xmm4
	movaps	%xmm4, %xmm6
	movss	%xmm5, -120(%rbp)
	movss	%xmm5, -76(%rbp)
	movss	-124(%rbp), %xmm5
	subss	%xmm5, %xmm6
	movss	%xmm6, -124(%rbp)
	movss	%xmm6, -72(%rbp)
	movaps	%xmm5, %xmm6
	addss	%xmm4, %xmm6
	movss	%xmm6, -128(%rbp)
	movss	%xmm6, -68(%rbp)
	call	puts
	.p2align 4,,10
	.p2align 3
.L23:
	movss	-96(%rbp,%rbx,4), %xmm0
	movl	%ebx, %esi
	movl	$.LC18, %edi
	movl	$1, %eax
	addq	$1, %rbx
	cvtps2pd	%xmm0, %xmm0
	call	printf
	cmpq	$8, %rbx
	jne	.L23
	movss	.LC6(%rip), %xmm0
	movss	-116(%rbp), %xmm4
	movss	-100(%rbp), %xmm1
	movss	-128(%rbp), %xmm5
	mulss	%xmm0, %xmm1
	cvtps2pd	%xmm4, %xmm4
	movss	-124(%rbp), %xmm6
	movss	-120(%rbp), %xmm7
	movl	$.LC25, %edi
	cvtps2pd	%xmm5, %xmm5
	xorb	%bl, %bl
	movapd	%xmm4, %xmm2
	mulsd	.LC24(%rip), %xmm4
	movss	%xmm1, (%r12)
	movsd	.LC20(%rip), %xmm3
	movsd	.LC19(%rip), %xmm1
	mulsd	%xmm5, %xmm3
	cvtps2pd	%xmm6, %xmm6
	mulsd	%xmm1, %xmm2
	cvtps2pd	%xmm7, %xmm7
	mulsd	%xmm5, %xmm1
	movapd	%xmm6, %xmm8
	mulsd	.LC23(%rip), %xmm6
	addsd	%xmm3, %xmm2
	movsd	.LC21(%rip), %xmm3
	addsd	%xmm4, %xmm1
	mulsd	%xmm7, %xmm3
	unpcklpd	%xmm2, %xmm2
	unpcklpd	%xmm1, %xmm1
	cvtpd2ps	%xmm2, %xmm2
	cvtpd2ps	%xmm1, %xmm1
	mulss	%xmm0, %xmm2
	mulss	%xmm0, %xmm1
	movss	%xmm2, 4(%r12)
	movss	-108(%rbp), %xmm2
	mulss	%xmm0, %xmm2
	movss	%xmm1, 28(%r12)
	movss	%xmm2, 8(%r12)
	movsd	.LC22(%rip), %xmm2
	mulsd	%xmm2, %xmm8
	mulsd	%xmm7, %xmm2
	addsd	%xmm8, %xmm3
	addsd	%xmm6, %xmm2
	unpcklpd	%xmm3, %xmm3
	unpcklpd	%xmm2, %xmm2
	cvtpd2ps	%xmm3, %xmm3
	cvtpd2ps	%xmm2, %xmm2
	mulss	%xmm0, %xmm3
	mulss	%xmm0, %xmm2
	movss	%xmm3, 12(%r12)
	movss	-104(%rbp), %xmm3
	movss	%xmm2, 20(%r12)
	mulss	%xmm0, %xmm3
	movss	-112(%rbp), %xmm2
	mulss	%xmm0, %xmm2
	movss	%xmm3, 16(%r12)
	movss	%xmm2, 24(%r12)
	call	puts
	.p2align 4,,10
	.p2align 3
.L25:
	movss	(%r12,%rbx,4), %xmm0
	movl	%ebx, %esi
	movl	$.LC26, %edi
	movl	$1, %eax
	addq	$1, %rbx
	cvtps2pd	%xmm0, %xmm0
	call	printf
	cmpq	$8, %rbx
	jne	.L25
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE30:
	.size	_Z15fast_float_dct8PfS_, .-_Z15fast_float_dct8PfS_
	.p2align 4,,15
	.globl	_Z17fast_float_dct8x8PA8_sS0_
	.type	_Z17fast_float_dct8x8PA8_sS0_, @function
_Z17fast_float_dct8x8PA8_sS0_:
.LFB28:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$576, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	call	mcount
	pxor	%xmm0, %xmm0
	xorl	%ebx, %ebx
	movdqu	(%rdi), %xmm1
	movq	%rsi, %r12
	movdqa	%xmm0, %xmm2
	movdqa	%xmm1, %xmm7
	pcmpgtw	%xmm1, %xmm2
	punpckhwd	%xmm2, %xmm1
	punpcklwd	%xmm2, %xmm7
	movdqa	%xmm0, %xmm2
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm1, -512(%rbp)
	cvtdq2ps	%xmm7, %xmm3
	movaps	%xmm3, -528(%rbp)
	movdqu	16(%rdi), %xmm1
	pcmpgtw	%xmm1, %xmm2
	movdqa	%xmm1, %xmm6
	punpckhwd	%xmm2, %xmm1
	punpcklwd	%xmm2, %xmm6
	movdqa	%xmm0, %xmm2
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm1, -480(%rbp)
	cvtdq2ps	%xmm6, %xmm3
	movaps	%xmm3, -496(%rbp)
	movdqu	32(%rdi), %xmm1
	pcmpgtw	%xmm1, %xmm2
	movdqa	%xmm1, %xmm7
	punpckhwd	%xmm2, %xmm1
	punpcklwd	%xmm2, %xmm7
	movdqa	%xmm0, %xmm2
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm1, -448(%rbp)
	cvtdq2ps	%xmm7, %xmm3
	movaps	%xmm3, -464(%rbp)
	movdqu	48(%rdi), %xmm1
	pcmpgtw	%xmm1, %xmm2
	movdqa	%xmm1, %xmm6
	punpckhwd	%xmm2, %xmm1
	punpcklwd	%xmm2, %xmm6
	movdqa	%xmm0, %xmm2
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm1, -416(%rbp)
	cvtdq2ps	%xmm6, %xmm3
	movaps	%xmm3, -432(%rbp)
	movdqu	64(%rdi), %xmm1
	pcmpgtw	%xmm1, %xmm2
	movdqa	%xmm1, %xmm7
	punpckhwd	%xmm2, %xmm1
	punpcklwd	%xmm2, %xmm7
	movdqa	%xmm0, %xmm2
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm1, -384(%rbp)
	cvtdq2ps	%xmm7, %xmm3
	movaps	%xmm3, -400(%rbp)
	movdqu	80(%rdi), %xmm1
	pcmpgtw	%xmm1, %xmm2
	movdqa	%xmm1, %xmm4
	punpckhwd	%xmm2, %xmm1
	punpcklwd	%xmm2, %xmm4
	movdqa	%xmm0, %xmm2
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm1, -352(%rbp)
	cvtdq2ps	%xmm4, %xmm3
	movaps	%xmm3, -368(%rbp)
	movdqu	96(%rdi), %xmm1
	pcmpgtw	%xmm1, %xmm2
	movdqa	%xmm1, %xmm6
	punpckhwd	%xmm2, %xmm1
	punpcklwd	%xmm2, %xmm6
	cvtdq2ps	%xmm1, %xmm1
	movaps	%xmm1, -320(%rbp)
	cvtdq2ps	%xmm6, %xmm3
	movaps	%xmm3, -336(%rbp)
	movdqu	112(%rdi), %xmm1
	pcmpgtw	%xmm1, %xmm0
	movdqa	%xmm1, %xmm7
	punpckhwd	%xmm0, %xmm1
	punpcklwd	%xmm0, %xmm7
	cvtdq2ps	%xmm1, %xmm0
	movaps	%xmm0, -288(%rbp)
	cvtdq2ps	%xmm7, %xmm2
	movaps	%xmm2, -304(%rbp)
	.p2align 4,,10
	.p2align 3
.L28:
	leaq	-272(%rbp), %rax
	leaq	(%rax,%rbx), %rsi
	leaq	-528(%rbp), %rax
	leaq	(%rax,%rbx), %rdi
	addq	$32, %rbx
	call	_Z15fast_float_dct8PfS_
	cmpq	$256, %rbx
	jne	.L28
	movaps	-272(%rbp), %xmm1
	xorw	%bx, %bx
	movaps	-256(%rbp), %xmm6
	movaps	%xmm1, %xmm3
	movaps	-240(%rbp), %xmm4
	shufps	$221, %xmm6, %xmm1
	shufps	$136, %xmm6, %xmm3
	movaps	-224(%rbp), %xmm2
	movaps	%xmm4, %xmm6
	movaps	-208(%rbp), %xmm0
	shufps	$136, %xmm2, %xmm6
	shufps	$221, %xmm2, %xmm4
	movaps	-192(%rbp), %xmm5
	movaps	%xmm0, %xmm2
	movaps	-176(%rbp), %xmm8
	shufps	$221, %xmm5, %xmm0
	shufps	$136, %xmm5, %xmm2
	movaps	-160(%rbp), %xmm7
	movaps	%xmm8, %xmm5
	shufps	$221, %xmm7, %xmm8
	shufps	$136, %xmm7, %xmm5
	movaps	%xmm3, %xmm7
	shufps	$221, %xmm6, %xmm3
	shufps	$136, %xmm6, %xmm7
	movaps	%xmm2, %xmm6
	shufps	$221, %xmm5, %xmm2
	shufps	$136, %xmm5, %xmm6
	movaps	%xmm1, %xmm5
	shufps	$221, %xmm4, %xmm1
	shufps	$136, %xmm4, %xmm5
	movaps	%xmm0, %xmm4
	shufps	$221, %xmm8, %xmm0
	shufps	$136, %xmm8, %xmm4
	movaps	%xmm7, %xmm8
	shufps	$221, %xmm6, %xmm7
	shufps	$136, %xmm6, %xmm8
	movaps	-128(%rbp), %xmm6
	movaps	%xmm7, -400(%rbp)
	movaps	%xmm8, -528(%rbp)
	movaps	%xmm5, %xmm8
	shufps	$221, %xmm4, %xmm5
	shufps	$136, %xmm4, %xmm8
	movaps	-112(%rbp), %xmm4
	movaps	%xmm8, -496(%rbp)
	movaps	%xmm3, %xmm8
	shufps	$221, %xmm2, %xmm3
	shufps	$136, %xmm2, %xmm8
	movaps	-96(%rbp), %xmm2
	movaps	%xmm8, -464(%rbp)
	movaps	%xmm1, %xmm8
	shufps	$221, %xmm0, %xmm1
	movaps	%xmm3, -336(%rbp)
	shufps	$136, %xmm0, %xmm8
	movaps	%xmm1, -304(%rbp)
	movaps	%xmm8, -432(%rbp)
	movaps	%xmm5, -368(%rbp)
	movaps	-144(%rbp), %xmm1
	movaps	%xmm1, %xmm3
	movaps	-80(%rbp), %xmm0
	shufps	$221, %xmm6, %xmm1
	shufps	$136, %xmm6, %xmm3
	movaps	%xmm4, %xmm6
	movaps	-64(%rbp), %xmm5
	shufps	$221, %xmm2, %xmm4
	shufps	$136, %xmm2, %xmm6
	movaps	-48(%rbp), %xmm8
	movaps	%xmm0, %xmm2
	movaps	-32(%rbp), %xmm7
	shufps	$221, %xmm5, %xmm0
	shufps	$136, %xmm5, %xmm2
	movaps	%xmm8, %xmm5
	shufps	$221, %xmm7, %xmm8
	shufps	$136, %xmm7, %xmm5
	movaps	%xmm3, %xmm7
	shufps	$221, %xmm6, %xmm3
	shufps	$136, %xmm6, %xmm7
	movaps	%xmm2, %xmm6
	shufps	$221, %xmm5, %xmm2
	shufps	$136, %xmm5, %xmm6
	movaps	%xmm1, %xmm5
	shufps	$221, %xmm4, %xmm1
	shufps	$136, %xmm4, %xmm5
	movaps	%xmm0, %xmm4
	shufps	$221, %xmm8, %xmm0
	shufps	$136, %xmm8, %xmm4
	movaps	%xmm7, %xmm8
	shufps	$221, %xmm6, %xmm7
	shufps	$136, %xmm6, %xmm8
	movaps	%xmm7, -384(%rbp)
	movaps	%xmm8, -512(%rbp)
	movaps	%xmm5, %xmm8
	shufps	$221, %xmm4, %xmm5
	shufps	$136, %xmm4, %xmm8
	movaps	%xmm5, -352(%rbp)
	movaps	%xmm8, -480(%rbp)
	movaps	%xmm3, %xmm8
	shufps	$221, %xmm2, %xmm3
	shufps	$136, %xmm2, %xmm8
	movaps	%xmm3, -320(%rbp)
	movaps	%xmm8, -448(%rbp)
	movaps	%xmm1, %xmm8
	shufps	$221, %xmm0, %xmm1
	shufps	$136, %xmm0, %xmm8
	movaps	%xmm1, -288(%rbp)
	movaps	%xmm8, -416(%rbp)
	.p2align 4,,10
	.p2align 3
.L30:
	leaq	-272(%rbp), %rax
	leaq	(%rax,%rbx), %rsi
	leaq	-528(%rbp), %rax
	leaq	(%rax,%rbx), %rdi
	addq	$32, %rbx
	call	_Z15fast_float_dct8PfS_
	cmpq	$256, %rbx
	jne	.L30
	movaps	-272(%rbp), %xmm1
	movaps	-256(%rbp), %xmm6
	movaps	%xmm1, %xmm3
	movaps	-240(%rbp), %xmm4
	shufps	$221, %xmm6, %xmm1
	shufps	$136, %xmm6, %xmm3
	movaps	-224(%rbp), %xmm2
	movaps	%xmm4, %xmm6
	movaps	-208(%rbp), %xmm0
	shufps	$136, %xmm2, %xmm6
	movaps	-192(%rbp), %xmm5
	shufps	$221, %xmm2, %xmm4
	movaps	%xmm0, %xmm2
	movaps	-176(%rbp), %xmm8
	shufps	$221, %xmm5, %xmm0
	shufps	$136, %xmm5, %xmm2
	movaps	-160(%rbp), %xmm7
	movaps	%xmm8, %xmm5
	shufps	$221, %xmm7, %xmm8
	shufps	$136, %xmm7, %xmm5
	movaps	%xmm3, %xmm7
	shufps	$221, %xmm6, %xmm3
	shufps	$136, %xmm6, %xmm7
	movaps	%xmm2, %xmm6
	shufps	$221, %xmm5, %xmm2
	shufps	$136, %xmm5, %xmm6
	movaps	%xmm1, %xmm5
	shufps	$221, %xmm4, %xmm1
	shufps	$136, %xmm4, %xmm5
	movaps	%xmm0, %xmm4
	shufps	$221, %xmm8, %xmm0
	movaps	%xmm1, %xmm9
	shufps	$136, %xmm8, %xmm4
	movaps	%xmm3, %xmm11
	shufps	$221, %xmm0, %xmm1
	movaps	%xmm7, %xmm15
	shufps	$221, %xmm2, %xmm3
	movaps	%xmm5, %xmm13
	shufps	$221, %xmm6, %xmm7
	movaps	%xmm1, -592(%rbp)
	shufps	$221, %xmm4, %xmm5
	movaps	%xmm3, -576(%rbp)
	shufps	$136, %xmm6, %xmm15
	shufps	$136, %xmm4, %xmm13
	shufps	$136, %xmm2, %xmm11
	movaps	-144(%rbp), %xmm1
	shufps	$136, %xmm0, %xmm9
	cvttps2dq	%xmm15, %xmm15
	cvttps2dq	%xmm13, %xmm13
	movaps	-128(%rbp), %xmm6
	cvttps2dq	%xmm11, %xmm11
	movaps	%xmm1, %xmm3
	cvttps2dq	%xmm9, %xmm9
	movaps	-112(%rbp), %xmm4
	shufps	$221, %xmm6, %xmm1
	shufps	$136, %xmm6, %xmm3
	movaps	-96(%rbp), %xmm2
	movaps	%xmm4, %xmm6
	movaps	-80(%rbp), %xmm0
	shufps	$136, %xmm2, %xmm6
	movaps	%xmm5, -560(%rbp)
	shufps	$221, %xmm2, %xmm4
	movaps	%xmm0, %xmm2
	movaps	%xmm7, -544(%rbp)
	movaps	-64(%rbp), %xmm5
	movaps	-48(%rbp), %xmm8
	shufps	$136, %xmm5, %xmm2
	movaps	-32(%rbp), %xmm7
	shufps	$221, %xmm5, %xmm0
	movaps	%xmm8, %xmm5
	shufps	$221, %xmm7, %xmm8
	shufps	$136, %xmm7, %xmm5
	movaps	%xmm3, %xmm7
	shufps	$221, %xmm6, %xmm3
	shufps	$136, %xmm6, %xmm7
	movaps	%xmm2, %xmm6
	shufps	$221, %xmm5, %xmm2
	shufps	$136, %xmm5, %xmm6
	movaps	%xmm1, %xmm5
	movaps	%xmm7, %xmm14
	shufps	$221, %xmm4, %xmm1
	movaps	%xmm3, %xmm10
	shufps	$136, %xmm4, %xmm5
	movaps	%xmm0, %xmm4
	shufps	$136, %xmm6, %xmm14
	shufps	$221, %xmm8, %xmm0
	shufps	$136, %xmm8, %xmm4
	movaps	%xmm1, %xmm8
	movaps	%xmm5, %xmm12
	cvttps2dq	%xmm14, %xmm14
	shufps	$136, %xmm0, %xmm8
	shufps	$221, %xmm0, %xmm1
	movdqa	%xmm15, %xmm0
	punpcklwd	%xmm14, %xmm15
	punpckhwd	%xmm14, %xmm0
	cvttps2dq	%xmm8, %xmm8
	shufps	$136, %xmm4, %xmm12
	cvttps2dq	%xmm1, %xmm1
	shufps	$136, %xmm2, %xmm10
	shufps	$221, %xmm2, %xmm3
	movdqa	%xmm15, %xmm2
	cvttps2dq	%xmm12, %xmm12
	punpcklwd	%xmm0, %xmm15
	punpckhwd	%xmm0, %xmm2
	movdqa	%xmm13, %xmm0
	cvttps2dq	%xmm10, %xmm10
	punpcklwd	%xmm12, %xmm13
	cvttps2dq	%xmm3, %xmm3
	punpckhwd	%xmm12, %xmm0
	punpcklwd	%xmm2, %xmm15
	movdqa	%xmm13, %xmm2
	shufps	$221, %xmm6, %xmm7
	punpcklwd	%xmm0, %xmm13
	punpckhwd	%xmm0, %xmm2
	movdqa	%xmm11, %xmm0
	punpcklwd	%xmm10, %xmm11
	cvttps2dq	%xmm7, %xmm7
	punpckhwd	%xmm10, %xmm0
	punpcklwd	%xmm2, %xmm13
	movdqa	%xmm11, %xmm2
	shufps	$221, %xmm4, %xmm5
	punpcklwd	%xmm0, %xmm11
	punpckhwd	%xmm0, %xmm2
	movdqa	%xmm9, %xmm0
	punpcklwd	%xmm8, %xmm9
	cvttps2dq	%xmm5, %xmm5
	punpckhwd	%xmm8, %xmm0
	punpcklwd	%xmm2, %xmm11
	movdqa	%xmm9, %xmm2
	punpcklwd	%xmm0, %xmm9
	punpckhwd	%xmm0, %xmm2
	cvttps2dq	-544(%rbp), %xmm0
	movdqu	%xmm15, (%r12)
	punpcklwd	%xmm2, %xmm9
	movdqa	%xmm0, %xmm2
	punpcklwd	%xmm7, %xmm0
	movdqu	%xmm13, 16(%r12)
	punpckhwd	%xmm7, %xmm2
	movdqa	%xmm0, %xmm4
	movdqu	%xmm11, 32(%r12)
	punpckhwd	%xmm2, %xmm4
	punpcklwd	%xmm2, %xmm0
	movdqu	%xmm9, 48(%r12)
	punpcklwd	%xmm4, %xmm0
	movdqu	%xmm0, 64(%r12)
	cvttps2dq	-560(%rbp), %xmm0
	movdqa	%xmm0, %xmm2
	punpcklwd	%xmm5, %xmm0
	punpckhwd	%xmm5, %xmm2
	movdqa	%xmm0, %xmm4
	punpcklwd	%xmm2, %xmm0
	punpckhwd	%xmm2, %xmm4
	punpcklwd	%xmm4, %xmm0
	movdqu	%xmm0, 80(%r12)
	cvttps2dq	-576(%rbp), %xmm0
	movdqa	%xmm0, %xmm2
	punpcklwd	%xmm3, %xmm0
	punpckhwd	%xmm3, %xmm2
	movdqa	%xmm0, %xmm3
	punpcklwd	%xmm2, %xmm0
	punpckhwd	%xmm2, %xmm3
	punpcklwd	%xmm3, %xmm0
	movdqu	%xmm0, 96(%r12)
	cvttps2dq	-592(%rbp), %xmm0
	movdqa	%xmm0, %xmm2
	punpcklwd	%xmm1, %xmm0
	punpckhwd	%xmm1, %xmm2
	movdqa	%xmm0, %xmm1
	punpcklwd	%xmm2, %xmm0
	punpckhwd	%xmm2, %xmm1
	punpcklwd	%xmm1, %xmm0
	movdqu	%xmm0, 112(%r12)
	addq	$576, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE28:
	.size	_Z17fast_float_dct8x8PA8_sS0_, .-_Z17fast_float_dct8x8PA8_sS0_
	.section	.rodata.str1.1
.LC29:
	.string	"input[%d] = %d\n"
.LC30:
	.string	"stage1[%d] = %d\n"
.LC31:
	.string	"stage2[%d] = %d\n"
.LC32:
	.string	"stage3[%d] = %d\n"
.LC33:
	.string	"output[%d] = %d\n"
	.text
	.p2align 4,,15
	.globl	_Z15fast_fixed_dct8PsS_
	.type	_Z15fast_fixed_dct8PsS_, @function
_Z15fast_fixed_dct8PsS_:
.LFB31:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	mcount
	pxor	%xmm1, %xmm1
	xorl	%r12d, %r12d
	movdqu	(%rdi), %xmm2
	movl	$.LC7, %edi
	movq	%rsi, %rbx
	pcmpgtw	%xmm2, %xmm1
	movdqa	%xmm2, %xmm3
	punpcklwd	%xmm1, %xmm3
	punpckhwd	%xmm1, %xmm2
	movdqa	%xmm3, %xmm0
	pslld	$4, %xmm2
	pslld	$4, %xmm0
	movdqa	%xmm0, %xmm1
	punpcklwd	%xmm2, %xmm0
	punpckhwd	%xmm2, %xmm1
	movdqa	%xmm0, %xmm2
	punpcklwd	%xmm1, %xmm0
	punpckhwd	%xmm1, %xmm2
	punpcklwd	%xmm2, %xmm0
	movdqa	%xmm0, -96(%rbp)
	call	puts
	.p2align 4,,10
	.p2align 3
.L33:
	movl	%r12d, %esi
	xorl	%eax, %eax
	movl	$.LC29, %edi
	movswl	-96(%rbp,%r12,2), %edx
	addq	$1, %r12
	sarl	$4, %edx
	call	printf
	cmpq	$8, %r12
	jne	.L33
	movzwl	-92(%rbp), %esi
	movzwl	-86(%rbp), %ecx
	xorb	%r12b, %r12b
	movzwl	-84(%rbp), %edi
	movzwl	-96(%rbp), %r10d
	movzwl	-82(%rbp), %r9d
	movzwl	-94(%rbp), %r8d
	movzwl	-90(%rbp), %edx
	leal	(%rcx,%rsi), %eax
	subl	%ecx, %esi
	movw	%si, -106(%rbp)
	movw	%si, -68(%rbp)
	movw	%ax, -102(%rbp)
	movw	%ax, -76(%rbp)
	leal	(%r9,%r10), %r15d
	movzwl	-88(%rbp), %eax
	leal	(%rdi,%r8), %r13d
	subl	%r9d, %r10d
	subl	%edi, %r8d
	movl	$.LC28, %edi
	movw	%r15w, -80(%rbp)
	movw	%r13w, -78(%rbp)
	movw	%r10w, -98(%rbp)
	movw	%r10w, -72(%rbp)
	movw	%r8w, -104(%rbp)
	leal	(%rax,%rdx), %r14d
	subl	%eax, %edx
	movw	%r8w, -70(%rbp)
	movw	%dx, -100(%rbp)
	movw	%dx, -66(%rbp)
	movw	%r14w, -74(%rbp)
	call	puts
	.p2align 4,,10
	.p2align 3
.L35:
	movswl	-80(%rbp,%r12,2), %edx
	movl	%r12d, %esi
	xorl	%eax, %eax
	movl	$.LC30, %edi
	addq	$1, %r12
	sarl	$4, %edx
	call	printf
	cmpq	$8, %r12
	jne	.L35
	movzwl	-102(%rbp), %ecx
	leal	(%r14,%r15), %eax
	subl	%r14d, %r15d
	movswl	-106(%rbp), %r14d
	movw	%r15w, -112(%rbp)
	movl	$.LC11, %edi
	movw	%ax, -108(%rbp)
	movw	%ax, -64(%rbp)
	xorb	%r12b, %r12b
	movw	%r15w, -58(%rbp)
	movl	%ecx, %eax
	addl	%r13d, %eax
	movl	%r14d, %r15d
	subl	%ecx, %r13d
	movw	%ax, -102(%rbp)
	movw	%ax, -62(%rbp)
	movzwl	-98(%rbp), %eax
	movw	%r13w, -110(%rbp)
	movw	%r13w, -60(%rbp)
	movw	%ax, -56(%rbp)
	movswl	-104(%rbp), %eax
	subl	%eax, %r15d
	addl	%eax, %r14d
	movzwl	-100(%rbp), %eax
	imull	$23170, %r15d, %r15d
	imull	$23170, %r14d, %r14d
	sarl	$15, %r15d
	movw	%ax, -50(%rbp)
	sarl	$15, %r14d
	movw	%r15w, -54(%rbp)
	movw	%r14w, -52(%rbp)
	call	puts
	.p2align 4,,10
	.p2align 3
.L37:
	movswl	-64(%rbp,%r12,2), %edx
	movl	%r12d, %esi
	xorl	%eax, %eax
	movl	$.LC31, %edi
	addq	$1, %r12
	sarl	$4, %edx
	call	printf
	cmpq	$8, %r12
	jne	.L37
	movswl	-102(%rbp), %eax
	movswl	-108(%rbp), %r12d
	movl	$.LC17, %edi
	movswl	-112(%rbp), %edx
	leal	(%r12,%rax), %r13d
	subl	%eax, %r12d
	movswl	-110(%rbp), %eax
	imull	$30273, %edx, %esi
	imull	$12539, %edx, %edx
	sarl	$15, %esi
	imull	$12539, %eax, %ecx
	sarl	$15, %edx
	imull	$-30273, %eax, %eax
	sarl	$15, %ecx
	sarl	$15, %eax
	addl	%esi, %ecx
	movzwl	-100(%rbp), %esi
	addl	%edx, %eax
	movw	%cx, -102(%rbp)
	movw	%cx, -76(%rbp)
	movw	%ax, -104(%rbp)
	movw	%ax, -74(%rbp)
	movzwl	-98(%rbp), %eax
	imull	$23170, %r13d, %r13d
	imull	$23170, %r12d, %r12d
	leal	(%rax,%r15), %ecx
	subl	%r15d, %eax
	sarl	$15, %r13d
	movswl	%ax, %r15d
	movl	%esi, %eax
	sarl	$15, %r12d
	subl	%r14d, %eax
	movw	%r13w, -80(%rbp)
	movw	%r12w, -78(%rbp)
	movw	%ax, -100(%rbp)
	movw	%ax, -68(%rbp)
	movl	%r14d, %eax
	addl	%esi, %eax
	xorl	%r14d, %r14d
	movw	%cx, -98(%rbp)
	movw	%cx, -72(%rbp)
	movw	%r15w, -70(%rbp)
	movw	%ax, -106(%rbp)
	movw	%ax, -66(%rbp)
	call	puts
	.p2align 4,,10
	.p2align 3
.L39:
	movswl	-80(%rbp,%r14,2), %edx
	movl	%r14d, %esi
	xorl	%eax, %eax
	movl	$.LC32, %edi
	addq	$1, %r14
	sarl	$4, %edx
	call	printf
	cmpq	$8, %r14
	jne	.L39
	movl	%r13d, %eax
	movswl	-106(%rbp), %r14d
	movzwl	-102(%rbp), %ecx
	shrw	$15, %ax
	movzwl	-104(%rbp), %r11d
	addl	%eax, %r13d
	movswl	-98(%rbp), %eax
	sarw	%r13w
	imull	$32138, %r14d, %r8d
	movl	%ecx, %edi
	movswl	%r13w, %r13d
	shrw	$15, %di
	addl	%ecx, %edi
	movswl	-100(%rbp), %ecx
	imull	$6393, %eax, %edx
	sarl	$15, %r8d
	sarw	%di
	movswl	%di, %edi
	sarl	$15, %edx
	addl	%edx, %r8d
	movl	%r8d, %edx
	shrw	$15, %dx
	addl	%edx, %r8d
	imull	$27245, %ecx, %esi
	sarw	%r8w
	imull	$-18205, %r15d, %edx
	movswl	%r8w, %r8d
	sarl	$15, %esi
	sarl	$15, %edx
	addl	%edx, %esi
	imull	$18205, %ecx, %ecx
	movl	%esi, %edx
	imull	$27245, %r15d, %r15d
	shrw	$15, %dx
	addl	%edx, %esi
	movl	%r12d, %edx
	sarl	$15, %ecx
	shrw	$15, %dx
	sarw	%si
	sarl	$15, %r15d
	addl	%edx, %r12d
	movswl	%si, %esi
	addl	%r15d, %ecx
	sarw	%r12w
	movl	%ecx, %edx
	movswl	%r12w, %r12d
	shrw	$15, %dx
	addl	%edx, %ecx
	movl	%r11d, %edx
	sarw	%cx
	shrw	$15, %dx
	sarl	$4, %r8d
	imull	$-32138, %eax, %eax
	addl	%r11d, %edx
	movswl	%cx, %ecx
	imull	$6393, %r14d, %r14d
	sarw	%dx
	sarl	$4, %edi
	movswl	%dx, %edx
	sarl	$4, %esi
	sarl	$4, %r12d
	sarl	$15, %eax
	sarl	$4, %ecx
	sarl	$4, %edx
	sarl	$15, %r14d
	sarl	$4, %r13d
	movw	%di, 4(%rbx)
	addl	%r14d, %eax
	movw	%r12w, 8(%rbx)
	movw	%r13w, (%rbx)
	movl	%eax, %r9d
	movw	%r8w, 2(%rbx)
	movw	%si, 6(%rbx)
	shrw	$15, %r9w
	movw	%cx, 10(%rbx)
	movw	%dx, 12(%rbx)
	addl	%r9d, %eax
	movl	$.LC25, %edi
	xorl	%r12d, %r12d
	sarw	%ax
	cwtl
	sarl	$4, %eax
	movw	%ax, 14(%rbx)
	call	puts
	.p2align 4,,10
	.p2align 3
.L41:
	movswl	(%rbx,%r12,2), %edx
	movl	%r12d, %esi
	xorl	%eax, %eax
	movl	$.LC33, %edi
	addq	$1, %r12
	call	printf
	cmpq	$8, %r12
	jne	.L41
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE31:
	.size	_Z15fast_fixed_dct8PsS_, .-_Z15fast_fixed_dct8PsS_
	.p2align 4,,15
	.globl	dct8x8
	.type	dct8x8, @function
dct8x8:
.LFB25:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	call	mcount
	xorl	%ebx, %ebx
	movq	%rdi, %r13
	movq	%rsi, %r12
	.p2align 4,,10
	.p2align 3
.L44:
	leaq	-288(%rbp), %rax
	leaq	0(%r13,%rbx), %rdi
	leaq	(%rax,%rbx), %rsi
	addq	$16, %rbx
	call	_Z15fast_fixed_dct8PsS_
	cmpq	$128, %rbx
	jne	.L44
	movdqa	-288(%rbp), %xmm5
	xorb	%bl, %bl
	movdqa	-272(%rbp), %xmm1
	movdqa	%xmm5, %xmm7
	movdqa	%xmm5, %xmm0
	movdqa	-256(%rbp), %xmm9
	punpcklwd	%xmm1, %xmm7
	punpckhwd	%xmm1, %xmm0
	movdqa	-240(%rbp), %xmm6
	punpcklwd	%xmm1, %xmm5
	movdqa	%xmm7, %xmm10
	movdqa	%xmm9, %xmm1
	punpcklwd	%xmm0, %xmm7
	punpckhwd	%xmm0, %xmm10
	movdqa	-224(%rbp), %xmm4
	punpcklwd	%xmm6, %xmm1
	punpcklwd	%xmm0, %xmm5
	movdqa	%xmm9, %xmm0
	punpcklwd	%xmm6, %xmm9
	punpckhwd	%xmm6, %xmm0
	movdqa	-208(%rbp), %xmm2
	punpcklwd	%xmm10, %xmm7
	punpckhwd	%xmm10, %xmm5
	movdqa	%xmm4, %xmm6
	movdqa	%xmm1, %xmm10
	punpcklwd	%xmm2, %xmm6
	movdqa	-192(%rbp), %xmm8
	punpckhwd	%xmm0, %xmm10
	punpcklwd	%xmm0, %xmm1
	movdqa	-176(%rbp), %xmm3
	punpcklwd	%xmm0, %xmm9
	movdqa	%xmm4, %xmm0
	punpcklwd	%xmm2, %xmm4
	punpcklwd	%xmm10, %xmm1
	punpckhwd	%xmm2, %xmm0
	movdqa	%xmm8, %xmm2
	punpckhwd	%xmm10, %xmm9
	movdqa	%xmm6, %xmm10
	punpckhwd	%xmm3, %xmm2
	punpcklwd	%xmm0, %xmm6
	punpckhwd	%xmm0, %xmm10
	punpcklwd	%xmm0, %xmm4
	movdqa	%xmm8, %xmm0
	punpcklwd	%xmm3, %xmm8
	punpcklwd	%xmm3, %xmm0
	movdqa	%xmm7, %xmm3
	punpcklwd	%xmm10, %xmm6
	punpckhwd	%xmm10, %xmm4
	movdqa	%xmm0, %xmm10
	punpcklwd	%xmm1, %xmm3
	punpcklwd	%xmm2, %xmm0
	punpckhwd	%xmm2, %xmm10
	punpcklwd	%xmm2, %xmm8
	movdqa	%xmm7, %xmm2
	punpcklwd	%xmm1, %xmm7
	punpckhwd	%xmm1, %xmm2
	movdqa	%xmm6, %xmm1
	punpcklwd	%xmm10, %xmm0
	punpckhwd	%xmm10, %xmm8
	movdqa	%xmm3, %xmm10
	punpcklwd	%xmm2, %xmm7
	punpckhwd	%xmm2, %xmm10
	punpcklwd	%xmm2, %xmm3
	movdqa	%xmm6, %xmm2
	punpckhwd	%xmm0, %xmm1
	punpcklwd	%xmm0, %xmm2
	punpcklwd	%xmm0, %xmm6
	movdqa	%xmm5, %xmm0
	punpcklwd	%xmm10, %xmm3
	punpckhwd	%xmm10, %xmm7
	movdqa	%xmm2, %xmm10
	punpcklwd	%xmm1, %xmm6
	punpckhwd	%xmm1, %xmm10
	movdqa	%xmm3, %xmm12
	punpcklwd	%xmm1, %xmm2
	movdqa	%xmm5, %xmm1
	punpckhwd	%xmm9, %xmm0
	punpcklwd	%xmm9, %xmm5
	punpcklwd	%xmm9, %xmm1
	movdqa	%xmm4, %xmm9
	punpcklwd	%xmm10, %xmm2
	punpckhwd	%xmm10, %xmm6
	movdqa	%xmm1, %xmm10
	punpcklwd	%xmm0, %xmm5
	punpcklwd	%xmm0, %xmm1
	punpckhwd	%xmm0, %xmm10
	movdqa	%xmm4, %xmm0
	punpckhwd	%xmm8, %xmm9
	punpcklwd	%xmm8, %xmm0
	punpcklwd	%xmm8, %xmm4
	movdqa	%xmm3, %xmm8
	punpckhwd	%xmm2, %xmm12
	punpcklwd	%xmm2, %xmm8
	punpcklwd	%xmm10, %xmm1
	punpckhwd	%xmm10, %xmm5
	movdqa	%xmm0, %xmm10
	movdqa	%xmm8, %xmm15
	punpcklwd	%xmm12, %xmm8
	movdqa	%xmm1, %xmm11
	punpckhwd	%xmm12, %xmm15
	punpckhwd	%xmm9, %xmm10
	punpcklwd	%xmm9, %xmm0
	punpcklwd	%xmm15, %xmm8
	punpcklwd	%xmm9, %xmm4
	movdqa	%xmm5, %xmm9
	punpcklwd	%xmm10, %xmm0
	movdqa	%xmm8, -160(%rbp)
	movdqa	%xmm1, %xmm8
	punpckhwd	%xmm0, %xmm11
	punpcklwd	%xmm0, %xmm8
	punpckhwd	%xmm10, %xmm4
	movdqa	%xmm7, %xmm10
	punpcklwd	%xmm2, %xmm3
	movdqa	%xmm8, %xmm14
	punpcklwd	%xmm11, %xmm8
	punpckhwd	%xmm6, %xmm10
	punpckhwd	%xmm11, %xmm14
	punpckhwd	%xmm4, %xmm9
	punpcklwd	%xmm0, %xmm1
	punpcklwd	%xmm14, %xmm8
	punpcklwd	%xmm12, %xmm3
	punpcklwd	%xmm11, %xmm1
	movdqa	%xmm8, -144(%rbp)
	movdqa	%xmm7, %xmm8
	punpcklwd	%xmm6, %xmm7
	punpcklwd	%xmm6, %xmm8
	punpckhwd	%xmm15, %xmm3
	punpcklwd	%xmm10, %xmm7
	movdqa	%xmm8, %xmm13
	punpcklwd	%xmm10, %xmm8
	punpckhwd	%xmm14, %xmm1
	punpckhwd	%xmm10, %xmm13
	movdqa	%xmm3, -96(%rbp)
	punpcklwd	%xmm13, %xmm8
	movdqa	%xmm13, -304(%rbp)
	movdqa	%xmm8, -128(%rbp)
	movdqa	%xmm5, %xmm8
	punpcklwd	%xmm4, %xmm5
	punpcklwd	%xmm4, %xmm8
	punpckhwd	-304(%rbp), %xmm7
	movdqa	%xmm1, -80(%rbp)
	movdqa	%xmm8, %xmm13
	punpcklwd	%xmm9, %xmm5
	punpckhwd	%xmm9, %xmm13
	punpcklwd	%xmm9, %xmm8
	movdqa	%xmm7, -64(%rbp)
	punpckhwd	%xmm13, %xmm5
	punpcklwd	%xmm13, %xmm8
	movdqa	%xmm5, -48(%rbp)
	movdqa	%xmm8, -112(%rbp)
	.p2align 4,,10
	.p2align 3
.L46:
	leaq	-288(%rbp), %rax
	leaq	(%rax,%rbx), %rsi
	leaq	-160(%rbp), %rax
	leaq	(%rax,%rbx), %rdi
	addq	$16, %rbx
	call	_Z15fast_fixed_dct8PsS_
	cmpq	$128, %rbx
	jne	.L46
	movdqa	-288(%rbp), %xmm5
	movdqa	-272(%rbp), %xmm1
	movdqa	%xmm5, %xmm7
	movdqa	%xmm5, %xmm0
	movdqa	-256(%rbp), %xmm9
	punpcklwd	%xmm1, %xmm7
	punpckhwd	%xmm1, %xmm0
	movdqa	-240(%rbp), %xmm6
	punpcklwd	%xmm1, %xmm5
	movdqa	%xmm7, %xmm10
	movdqa	%xmm9, %xmm1
	punpcklwd	%xmm0, %xmm7
	punpckhwd	%xmm0, %xmm10
	movdqa	-224(%rbp), %xmm4
	punpcklwd	%xmm6, %xmm1
	punpcklwd	%xmm0, %xmm5
	movdqa	%xmm9, %xmm0
	punpcklwd	%xmm6, %xmm9
	punpckhwd	%xmm6, %xmm0
	movdqa	-208(%rbp), %xmm2
	punpcklwd	%xmm10, %xmm7
	punpckhwd	%xmm10, %xmm5
	movdqa	%xmm4, %xmm6
	movdqa	%xmm1, %xmm10
	punpcklwd	%xmm2, %xmm6
	movdqa	-192(%rbp), %xmm8
	punpckhwd	%xmm0, %xmm10
	punpcklwd	%xmm0, %xmm1
	movdqa	-176(%rbp), %xmm3
	punpcklwd	%xmm0, %xmm9
	movdqa	%xmm4, %xmm0
	punpcklwd	%xmm2, %xmm4
	punpcklwd	%xmm10, %xmm1
	punpckhwd	%xmm2, %xmm0
	movdqa	%xmm8, %xmm2
	punpckhwd	%xmm10, %xmm9
	movdqa	%xmm6, %xmm10
	punpckhwd	%xmm3, %xmm2
	punpcklwd	%xmm0, %xmm6
	punpckhwd	%xmm0, %xmm10
	punpcklwd	%xmm0, %xmm4
	movdqa	%xmm8, %xmm0
	punpcklwd	%xmm3, %xmm8
	punpcklwd	%xmm3, %xmm0
	movdqa	%xmm7, %xmm3
	punpcklwd	%xmm10, %xmm6
	punpckhwd	%xmm10, %xmm4
	movdqa	%xmm0, %xmm10
	punpcklwd	%xmm1, %xmm3
	punpcklwd	%xmm2, %xmm0
	punpckhwd	%xmm2, %xmm10
	punpcklwd	%xmm2, %xmm8
	movdqa	%xmm7, %xmm2
	punpcklwd	%xmm1, %xmm7
	punpckhwd	%xmm1, %xmm2
	movdqa	%xmm6, %xmm1
	punpcklwd	%xmm10, %xmm0
	punpckhwd	%xmm10, %xmm8
	movdqa	%xmm3, %xmm10
	punpcklwd	%xmm2, %xmm7
	punpckhwd	%xmm2, %xmm10
	punpcklwd	%xmm2, %xmm3
	movdqa	%xmm6, %xmm2
	punpckhwd	%xmm0, %xmm1
	punpcklwd	%xmm0, %xmm2
	punpcklwd	%xmm0, %xmm6
	movdqa	%xmm5, %xmm0
	punpcklwd	%xmm10, %xmm3
	punpckhwd	%xmm10, %xmm7
	movdqa	%xmm2, %xmm10
	punpcklwd	%xmm1, %xmm6
	punpckhwd	%xmm1, %xmm10
	movdqa	%xmm3, %xmm12
	punpcklwd	%xmm1, %xmm2
	movdqa	%xmm5, %xmm1
	punpckhwd	%xmm9, %xmm0
	punpcklwd	%xmm9, %xmm5
	punpcklwd	%xmm9, %xmm1
	movdqa	%xmm4, %xmm9
	punpcklwd	%xmm10, %xmm2
	punpckhwd	%xmm10, %xmm6
	movdqa	%xmm1, %xmm10
	punpcklwd	%xmm0, %xmm5
	punpcklwd	%xmm0, %xmm1
	punpckhwd	%xmm0, %xmm10
	movdqa	%xmm4, %xmm0
	punpckhwd	%xmm8, %xmm9
	punpcklwd	%xmm8, %xmm0
	punpcklwd	%xmm8, %xmm4
	movdqa	%xmm3, %xmm8
	punpckhwd	%xmm2, %xmm12
	punpcklwd	%xmm2, %xmm8
	punpcklwd	%xmm10, %xmm1
	punpckhwd	%xmm10, %xmm5
	movdqa	%xmm0, %xmm10
	movdqa	%xmm8, %xmm15
	punpcklwd	%xmm12, %xmm8
	movdqa	%xmm1, %xmm11
	punpckhwd	%xmm12, %xmm15
	punpckhwd	%xmm9, %xmm10
	punpcklwd	%xmm9, %xmm0
	punpcklwd	%xmm15, %xmm8
	punpcklwd	%xmm9, %xmm4
	movdqa	%xmm5, %xmm9
	punpcklwd	%xmm10, %xmm0
	movdqu	%xmm8, (%r12)
	movdqa	%xmm1, %xmm8
	punpckhwd	%xmm0, %xmm11
	punpcklwd	%xmm0, %xmm8
	punpckhwd	%xmm10, %xmm4
	movdqa	%xmm7, %xmm10
	punpcklwd	%xmm2, %xmm3
	movdqa	%xmm8, %xmm14
	punpcklwd	%xmm11, %xmm8
	punpckhwd	%xmm6, %xmm10
	punpckhwd	%xmm11, %xmm14
	punpckhwd	%xmm4, %xmm9
	punpcklwd	%xmm0, %xmm1
	punpcklwd	%xmm14, %xmm8
	punpcklwd	%xmm12, %xmm3
	punpcklwd	%xmm11, %xmm1
	movdqu	%xmm8, 16(%r12)
	movdqa	%xmm7, %xmm8
	punpcklwd	%xmm6, %xmm7
	punpcklwd	%xmm6, %xmm8
	punpckhwd	%xmm15, %xmm3
	punpcklwd	%xmm10, %xmm7
	movdqa	%xmm8, %xmm13
	punpcklwd	%xmm10, %xmm8
	punpckhwd	%xmm14, %xmm1
	punpckhwd	%xmm10, %xmm13
	movdqu	%xmm3, 64(%r12)
	punpcklwd	%xmm13, %xmm8
	movdqa	%xmm13, -304(%rbp)
	movdqu	%xmm8, 32(%r12)
	movdqa	%xmm5, %xmm8
	punpcklwd	%xmm4, %xmm5
	punpcklwd	%xmm4, %xmm8
	punpckhwd	-304(%rbp), %xmm7
	movdqu	%xmm1, 80(%r12)
	movdqa	%xmm8, %xmm13
	punpcklwd	%xmm9, %xmm5
	punpckhwd	%xmm9, %xmm13
	punpcklwd	%xmm9, %xmm8
	movdqu	%xmm7, 96(%r12)
	punpckhwd	%xmm13, %xmm5
	punpcklwd	%xmm13, %xmm8
	movdqu	%xmm5, 112(%r12)
	movdqu	%xmm8, 48(%r12)
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25:
	.size	dct8x8, .-dct8x8
	.p2align 4,,15
	.globl	_Z17fast_fixed_dct8x8PA8_sS0_
	.type	_Z17fast_fixed_dct8x8PA8_sS0_, @function
_Z17fast_fixed_dct8x8PA8_sS0_:
.LFB33:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	call	mcount
	xorl	%ebx, %ebx
	movq	%rdi, %r13
	movq	%rsi, %r12
	.p2align 4,,10
	.p2align 3
.L49:
	leaq	-288(%rbp), %rax
	leaq	0(%r13,%rbx), %rdi
	leaq	(%rax,%rbx), %rsi
	addq	$16, %rbx
	call	_Z15fast_fixed_dct8PsS_
	cmpq	$128, %rbx
	jne	.L49
	movdqa	-288(%rbp), %xmm5
	xorb	%bl, %bl
	movdqa	-272(%rbp), %xmm1
	movdqa	%xmm5, %xmm7
	movdqa	%xmm5, %xmm0
	movdqa	-256(%rbp), %xmm9
	punpcklwd	%xmm1, %xmm7
	punpckhwd	%xmm1, %xmm0
	movdqa	-240(%rbp), %xmm6
	punpcklwd	%xmm1, %xmm5
	movdqa	%xmm7, %xmm10
	movdqa	%xmm9, %xmm1
	punpcklwd	%xmm0, %xmm7
	punpckhwd	%xmm0, %xmm10
	movdqa	-224(%rbp), %xmm4
	punpcklwd	%xmm6, %xmm1
	punpcklwd	%xmm0, %xmm5
	movdqa	%xmm9, %xmm0
	punpcklwd	%xmm6, %xmm9
	punpckhwd	%xmm6, %xmm0
	movdqa	-208(%rbp), %xmm2
	punpcklwd	%xmm10, %xmm7
	punpckhwd	%xmm10, %xmm5
	movdqa	%xmm4, %xmm6
	movdqa	%xmm1, %xmm10
	punpcklwd	%xmm2, %xmm6
	movdqa	-192(%rbp), %xmm8
	punpckhwd	%xmm0, %xmm10
	punpcklwd	%xmm0, %xmm1
	movdqa	-176(%rbp), %xmm3
	punpcklwd	%xmm0, %xmm9
	movdqa	%xmm4, %xmm0
	punpcklwd	%xmm2, %xmm4
	punpcklwd	%xmm10, %xmm1
	punpckhwd	%xmm2, %xmm0
	movdqa	%xmm8, %xmm2
	punpckhwd	%xmm10, %xmm9
	movdqa	%xmm6, %xmm10
	punpckhwd	%xmm3, %xmm2
	punpcklwd	%xmm0, %xmm6
	punpckhwd	%xmm0, %xmm10
	punpcklwd	%xmm0, %xmm4
	movdqa	%xmm8, %xmm0
	punpcklwd	%xmm3, %xmm8
	punpcklwd	%xmm3, %xmm0
	movdqa	%xmm7, %xmm3
	punpcklwd	%xmm10, %xmm6
	punpckhwd	%xmm10, %xmm4
	movdqa	%xmm0, %xmm10
	punpcklwd	%xmm1, %xmm3
	punpcklwd	%xmm2, %xmm0
	punpckhwd	%xmm2, %xmm10
	punpcklwd	%xmm2, %xmm8
	movdqa	%xmm7, %xmm2
	punpcklwd	%xmm1, %xmm7
	punpckhwd	%xmm1, %xmm2
	movdqa	%xmm6, %xmm1
	punpcklwd	%xmm10, %xmm0
	punpckhwd	%xmm10, %xmm8
	movdqa	%xmm3, %xmm10
	punpcklwd	%xmm2, %xmm7
	punpckhwd	%xmm2, %xmm10
	punpcklwd	%xmm2, %xmm3
	movdqa	%xmm6, %xmm2
	punpckhwd	%xmm0, %xmm1
	punpcklwd	%xmm0, %xmm2
	punpcklwd	%xmm0, %xmm6
	movdqa	%xmm5, %xmm0
	punpcklwd	%xmm10, %xmm3
	punpckhwd	%xmm10, %xmm7
	movdqa	%xmm2, %xmm10
	punpcklwd	%xmm1, %xmm6
	punpckhwd	%xmm1, %xmm10
	movdqa	%xmm3, %xmm12
	punpcklwd	%xmm1, %xmm2
	movdqa	%xmm5, %xmm1
	punpckhwd	%xmm9, %xmm0
	punpcklwd	%xmm9, %xmm5
	punpcklwd	%xmm9, %xmm1
	movdqa	%xmm4, %xmm9
	punpcklwd	%xmm10, %xmm2
	punpckhwd	%xmm10, %xmm6
	movdqa	%xmm1, %xmm10
	punpcklwd	%xmm0, %xmm5
	punpcklwd	%xmm0, %xmm1
	punpckhwd	%xmm0, %xmm10
	movdqa	%xmm4, %xmm0
	punpckhwd	%xmm8, %xmm9
	punpcklwd	%xmm8, %xmm0
	punpcklwd	%xmm8, %xmm4
	movdqa	%xmm3, %xmm8
	punpckhwd	%xmm2, %xmm12
	punpcklwd	%xmm2, %xmm8
	punpcklwd	%xmm10, %xmm1
	punpckhwd	%xmm10, %xmm5
	movdqa	%xmm0, %xmm10
	movdqa	%xmm8, %xmm15
	punpcklwd	%xmm12, %xmm8
	movdqa	%xmm1, %xmm11
	punpckhwd	%xmm12, %xmm15
	punpckhwd	%xmm9, %xmm10
	punpcklwd	%xmm9, %xmm0
	punpcklwd	%xmm15, %xmm8
	punpcklwd	%xmm9, %xmm4
	movdqa	%xmm5, %xmm9
	punpcklwd	%xmm10, %xmm0
	movdqa	%xmm8, -160(%rbp)
	movdqa	%xmm1, %xmm8
	punpckhwd	%xmm0, %xmm11
	punpcklwd	%xmm0, %xmm8
	punpckhwd	%xmm10, %xmm4
	movdqa	%xmm7, %xmm10
	punpcklwd	%xmm2, %xmm3
	movdqa	%xmm8, %xmm14
	punpcklwd	%xmm11, %xmm8
	punpckhwd	%xmm6, %xmm10
	punpckhwd	%xmm11, %xmm14
	punpckhwd	%xmm4, %xmm9
	punpcklwd	%xmm0, %xmm1
	punpcklwd	%xmm14, %xmm8
	punpcklwd	%xmm12, %xmm3
	punpcklwd	%xmm11, %xmm1
	movdqa	%xmm8, -144(%rbp)
	movdqa	%xmm7, %xmm8
	punpcklwd	%xmm6, %xmm7
	punpcklwd	%xmm6, %xmm8
	punpckhwd	%xmm15, %xmm3
	punpcklwd	%xmm10, %xmm7
	movdqa	%xmm8, %xmm13
	punpcklwd	%xmm10, %xmm8
	punpckhwd	%xmm14, %xmm1
	punpckhwd	%xmm10, %xmm13
	movdqa	%xmm3, -96(%rbp)
	punpcklwd	%xmm13, %xmm8
	movdqa	%xmm13, -304(%rbp)
	movdqa	%xmm8, -128(%rbp)
	movdqa	%xmm5, %xmm8
	punpcklwd	%xmm4, %xmm5
	punpcklwd	%xmm4, %xmm8
	punpckhwd	-304(%rbp), %xmm7
	movdqa	%xmm1, -80(%rbp)
	movdqa	%xmm8, %xmm13
	punpcklwd	%xmm9, %xmm5
	punpckhwd	%xmm9, %xmm13
	punpcklwd	%xmm9, %xmm8
	movdqa	%xmm7, -64(%rbp)
	punpckhwd	%xmm13, %xmm5
	punpcklwd	%xmm13, %xmm8
	movdqa	%xmm5, -48(%rbp)
	movdqa	%xmm8, -112(%rbp)
	.p2align 4,,10
	.p2align 3
.L51:
	leaq	-288(%rbp), %rax
	leaq	(%rax,%rbx), %rsi
	leaq	-160(%rbp), %rax
	leaq	(%rax,%rbx), %rdi
	addq	$16, %rbx
	call	_Z15fast_fixed_dct8PsS_
	cmpq	$128, %rbx
	jne	.L51
	movdqa	-288(%rbp), %xmm5
	movdqa	-272(%rbp), %xmm1
	movdqa	%xmm5, %xmm7
	movdqa	%xmm5, %xmm0
	movdqa	-256(%rbp), %xmm9
	punpcklwd	%xmm1, %xmm7
	punpckhwd	%xmm1, %xmm0
	movdqa	-240(%rbp), %xmm6
	punpcklwd	%xmm1, %xmm5
	movdqa	%xmm7, %xmm10
	movdqa	%xmm9, %xmm1
	punpcklwd	%xmm0, %xmm7
	punpckhwd	%xmm0, %xmm10
	movdqa	-224(%rbp), %xmm4
	punpcklwd	%xmm6, %xmm1
	punpcklwd	%xmm0, %xmm5
	movdqa	%xmm9, %xmm0
	punpcklwd	%xmm6, %xmm9
	punpckhwd	%xmm6, %xmm0
	movdqa	-208(%rbp), %xmm2
	punpcklwd	%xmm10, %xmm7
	punpckhwd	%xmm10, %xmm5
	movdqa	%xmm4, %xmm6
	movdqa	%xmm1, %xmm10
	punpcklwd	%xmm2, %xmm6
	movdqa	-192(%rbp), %xmm8
	punpckhwd	%xmm0, %xmm10
	punpcklwd	%xmm0, %xmm1
	movdqa	-176(%rbp), %xmm3
	punpcklwd	%xmm0, %xmm9
	movdqa	%xmm4, %xmm0
	punpcklwd	%xmm2, %xmm4
	punpcklwd	%xmm10, %xmm1
	punpckhwd	%xmm2, %xmm0
	movdqa	%xmm8, %xmm2
	punpckhwd	%xmm10, %xmm9
	movdqa	%xmm6, %xmm10
	punpckhwd	%xmm3, %xmm2
	punpcklwd	%xmm0, %xmm6
	punpckhwd	%xmm0, %xmm10
	punpcklwd	%xmm0, %xmm4
	movdqa	%xmm8, %xmm0
	punpcklwd	%xmm3, %xmm8
	punpcklwd	%xmm3, %xmm0
	movdqa	%xmm7, %xmm3
	punpcklwd	%xmm10, %xmm6
	punpckhwd	%xmm10, %xmm4
	movdqa	%xmm0, %xmm10
	punpcklwd	%xmm1, %xmm3
	punpcklwd	%xmm2, %xmm0
	punpckhwd	%xmm2, %xmm10
	punpcklwd	%xmm2, %xmm8
	movdqa	%xmm7, %xmm2
	punpcklwd	%xmm1, %xmm7
	punpckhwd	%xmm1, %xmm2
	movdqa	%xmm6, %xmm1
	punpcklwd	%xmm10, %xmm0
	punpckhwd	%xmm10, %xmm8
	movdqa	%xmm3, %xmm10
	punpcklwd	%xmm2, %xmm7
	punpckhwd	%xmm2, %xmm10
	punpcklwd	%xmm2, %xmm3
	movdqa	%xmm6, %xmm2
	punpckhwd	%xmm0, %xmm1
	punpcklwd	%xmm0, %xmm2
	punpcklwd	%xmm0, %xmm6
	movdqa	%xmm5, %xmm0
	punpcklwd	%xmm10, %xmm3
	punpckhwd	%xmm10, %xmm7
	movdqa	%xmm2, %xmm10
	punpcklwd	%xmm1, %xmm6
	punpckhwd	%xmm1, %xmm10
	movdqa	%xmm3, %xmm12
	punpcklwd	%xmm1, %xmm2
	movdqa	%xmm5, %xmm1
	punpckhwd	%xmm9, %xmm0
	punpcklwd	%xmm9, %xmm5
	punpcklwd	%xmm9, %xmm1
	movdqa	%xmm4, %xmm9
	punpcklwd	%xmm10, %xmm2
	punpckhwd	%xmm10, %xmm6
	movdqa	%xmm1, %xmm10
	punpcklwd	%xmm0, %xmm5
	punpcklwd	%xmm0, %xmm1
	punpckhwd	%xmm0, %xmm10
	movdqa	%xmm4, %xmm0
	punpckhwd	%xmm8, %xmm9
	punpcklwd	%xmm8, %xmm0
	punpcklwd	%xmm8, %xmm4
	movdqa	%xmm3, %xmm8
	punpckhwd	%xmm2, %xmm12
	punpcklwd	%xmm2, %xmm8
	punpcklwd	%xmm10, %xmm1
	punpckhwd	%xmm10, %xmm5
	movdqa	%xmm0, %xmm10
	movdqa	%xmm8, %xmm15
	punpcklwd	%xmm12, %xmm8
	movdqa	%xmm1, %xmm11
	punpckhwd	%xmm12, %xmm15
	punpckhwd	%xmm9, %xmm10
	punpcklwd	%xmm9, %xmm0
	punpcklwd	%xmm15, %xmm8
	punpcklwd	%xmm9, %xmm4
	movdqa	%xmm5, %xmm9
	punpcklwd	%xmm10, %xmm0
	movdqu	%xmm8, (%r12)
	movdqa	%xmm1, %xmm8
	punpckhwd	%xmm0, %xmm11
	punpcklwd	%xmm0, %xmm8
	punpckhwd	%xmm10, %xmm4
	movdqa	%xmm7, %xmm10
	punpcklwd	%xmm2, %xmm3
	movdqa	%xmm8, %xmm14
	punpcklwd	%xmm11, %xmm8
	punpckhwd	%xmm6, %xmm10
	punpckhwd	%xmm11, %xmm14
	punpckhwd	%xmm4, %xmm9
	punpcklwd	%xmm0, %xmm1
	punpcklwd	%xmm14, %xmm8
	punpcklwd	%xmm12, %xmm3
	punpcklwd	%xmm11, %xmm1
	movdqu	%xmm8, 16(%r12)
	movdqa	%xmm7, %xmm8
	punpcklwd	%xmm6, %xmm7
	punpcklwd	%xmm6, %xmm8
	punpckhwd	%xmm15, %xmm3
	punpcklwd	%xmm10, %xmm7
	movdqa	%xmm8, %xmm13
	punpcklwd	%xmm10, %xmm8
	punpckhwd	%xmm14, %xmm1
	punpckhwd	%xmm10, %xmm13
	movdqu	%xmm3, 64(%r12)
	punpcklwd	%xmm13, %xmm8
	movdqa	%xmm13, -304(%rbp)
	movdqu	%xmm8, 32(%r12)
	movdqa	%xmm5, %xmm8
	punpcklwd	%xmm4, %xmm5
	punpcklwd	%xmm4, %xmm8
	punpckhwd	-304(%rbp), %xmm7
	movdqu	%xmm1, 80(%r12)
	movdqa	%xmm8, %xmm13
	punpcklwd	%xmm9, %xmm5
	punpckhwd	%xmm9, %xmm13
	punpcklwd	%xmm9, %xmm8
	movdqu	%xmm7, 96(%r12)
	punpckhwd	%xmm13, %xmm5
	punpcklwd	%xmm13, %xmm8
	movdqu	%xmm5, 112(%r12)
	movdqu	%xmm8, 48(%r12)
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE33:
	.size	_Z17fast_fixed_dct8x8PA8_sS0_, .-_Z17fast_fixed_dct8x8PA8_sS0_
	.globl	Av
	.data
	.align 32
	.type	Av, @object
	.size	Av, 256
Av:
	.long	1040187392
	.long	1043662066
	.long	1043662066
	.long	1043662066
	.long	1043662066
	.long	1043662066
	.long	1043662066
	.long	1043662066
	.long	1043662066
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1043662066
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1043662066
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1043662066
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1043662066
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1043662066
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1043662066
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.long	1048576000
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1072693248
	.align 8
.LC2:
	.long	776530087
	.long	1074340351
	.align 8
.LC3:
	.long	0
	.long	1068498944
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC4:
	.long	1060439283
	.align 4
.LC6:
	.long	1056964608
	.section	.rodata.cst8
	.align 8
.LC10:
	.long	769658139
	.long	1072078992
	.align 8
.LC13:
	.long	769658139
	.long	-1075404656
	.align 8
.LC14:
	.long	1094014070
	.long	1071152596
	.align 8
.LC15:
	.long	3391993372
	.long	1072533612
	.align 8
.LC16:
	.long	3391993372
	.long	-1074950036
	.align 8
.LC19:
	.long	1807322238
	.long	1070135561
	.align 8
.LC20:
	.long	3172091046
	.long	1072652940
	.align 8
.LC21:
	.long	3041524040
	.long	-1075722417
	.align 8
.LC22:
	.long	8246337
	.long	1072339794
	.align 8
.LC23:
	.long	3041524040
	.long	1071761231
	.align 8
.LC24:
	.long	3172091046
	.long	-1074830708
	.ident	"GCC: (GNU) 4.8.2 20131212 (Red Hat 4.8.2-7)"
	.section	.note.GNU-stack,"",@progbits
