#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "dct.h"

#define VAL00 (0.5/4.0)
#define VAL0x (0.7071067/4.0)
#define VALx0 (0.7071067/4.0)
#define VALxx (1.0/4.0)

// Coefs pour le fast fixed au format Q0_15
#define C0_f 46340
#define C1_f 32138
#define C2_f 30273
#define C3_f 27245
#define C4_f 23170
#define C5_f 18205
#define C6_f 12539
#define C7_f 6393

#define C0 (2*(0.7071068))
#define C1 (2*(0.49039))
#define C2 (2*(0.46194))
#define C3 (2*(0.41573))
#define C4 (2*(0.35355))
#define C5 (2*(0.27779))
#define C6 (2*(0.19134))
#define C7 (2*(0.09755))

float Av[8][8] = {
		{VAL00, VAL0x, VAL0x, VAL0x, VAL0x, VAL0x, VAL0x, VAL0x},
		{VALx0, VALxx, VALxx, VALxx, VALxx, VALxx, VALxx, VALxx},
		{VALx0, VALxx, VALxx, VALxx, VALxx, VALxx, VALxx, VALxx},
		{VALx0, VALxx, VALxx, VALxx, VALxx, VALxx, VALxx, VALxx},
		{VALx0, VALxx, VALxx, VALxx, VALxx, VALxx, VALxx, VALxx},
		{VALx0, VALxx, VALxx, VALxx, VALxx, VALxx, VALxx, VALxx},
		{VALx0, VALxx, VALxx, VALxx, VALxx, VALxx, VALxx, VALxx},
		{VALx0, VALxx, VALxx, VALxx, VALxx, VALxx, VALxx, VALxx}
};

#define PI 3.1416

void slow_float_dct8x8(short pixel[8][8], short data[8][8]);
void fast_float_dct8x8(short pixel[8][8], short data[8][8]);
void fast_fixed_dct8x8(short pixel[8][8], short data[8][8]);
void slow_float_dct8(float in[8], float out[8]);
void fast_float_dct8(float in[8], float out[8]);
void fast_fixed_dct8(short in[8], short out[8]);


void dct8x8(short pixel[8][8], short data[8][8]) {
//	slow_float_dct8x8(pixel,data);
//	fast_float_dct8x8(pixel,data);
	fast_fixed_dct8x8(pixel,data);
}



void slow_float_dct8x8(short pixel[8][8], short data[8][8])
{
	int i,j,u, v;
	short tmp[8][8];

	for (v = 0; v < 8; v++) {
		for (u = 0; u < 8; u++) {
			double res_b = 0.0;
			for (i = 0; i < 8; i++) {
				for (j = 0; j < 8; j++) {
					res_b = res_b + Av[u][v] * cos(((2 * i + 1) * u * PI) / 16.0)
							* cos(((2 * j + 1) * v * PI) / 16.0)
							* pixel[i][j];
				}
			}
			tmp[u][v]=res_b;
		}
	}

	for (v=0; v<8; v++) {
		for (u=0; u<8; u++) {
			data[u][v] =  tmp[u][v];
		}
	}
}

static void transpose_lignes_colonnes(float in[8][8], float out[8][8]) {
	int i, j;

	for (i = 0; i < 8; i++) {
		for (j = 0; j < 8; j++)
			out[j][i] = in[i][j];
	}
}

void fast_float_dct8x8(short pixel[8][8], short data[8][8])
{
	float ping[8][8], pong[8][8];
	int i, j;

	// pixel{short} => ping{float}
	for (i = 0; i < 8; i++) {
		for (j = 0; j < 8; j++)
			ping[i][j] = (float) pixel[i][j];
	}
	
	for (i = 0; i < 8; i++) {
		//slow_float_dct8(ping[i], pong[i]);
		fast_float_dct8(ping[i], pong[i]);
	}

	transpose_lignes_colonnes(pong, ping);

	for (i = 0; i < 8; i++) {
		//slow_float_dct8(ping[i], pong[i]);
		fast_float_dct8(ping[i], pong[i]);
	}

	transpose_lignes_colonnes(pong, ping);

	// ping{float} => data{short}
	for (i = 0; i < 8; i++) {
		for (j = 0; j < 8; j++)
			data[i][j] = (short) ping[i][j];
	}
}


void slow_float_dct8(float in[8], float out[8]) {
	float sum, Cu0 = 1 / sqrt(2), tmp;
	int i, u;

	for (u = 0; u < 8; u++) {
		sum = 0;

		for (i = 0; i < 8; i++) {
			tmp = in[i] * cos(((2 * i + 1) * u * PI) / 16);

			if (u == 0)
				tmp *= Cu0;

			sum += tmp;
		}

		out[u] = sum / 2;
	}
}

void fast_float_dct8(float in[8], float out[8]) {
	int k,n,i;
	float tmp[8];
	float tmp2[8];

#ifdef TRACE
	printf("\n==== Input ====\n");
	for (i=0;i<8;i++) {
		printf("input[%d] = %f\n",i,in[i]);
	}
#endif

	// Etage 1 à compléter
	for (i=0; i<8; i++) {
		float t = in[i];
		if(i>3) 
			t = -t;

		tmp[i] = in[7-i] + t;
	}

#ifdef TRACE
	printf("\n==== Stage 1 ====\n");
	for (i=0;i<8;i++) {
		printf("stage1[%d] = %f\n",i,tmp[i]);
	}
#endif

	// Etage 2 à compléter
	tmp2[0] = tmp[0] + tmp[3];
	tmp2[1] = tmp[1] + tmp[2];
	tmp2[2] = tmp[1] - tmp[2];
	tmp2[3] = tmp[0] - tmp[3];
	tmp2[4] = tmp[4];
	tmp2[5] = (tmp[6] - tmp[5]) * C4;
	tmp2[6] = (tmp[5] + tmp[6]) * C4;
	tmp2[7] = tmp[7];

#ifdef TRACE
	printf("\n==== Stage 2 ====\n");
	for (i=0;i<8;i++) {
		printf("stage2[%d] = %f\n",i,tmp2[i]);
	}
#endif

	tmp[0] = tmp2[0] * C4 + tmp2[1] * C4;
	tmp[1] = tmp2[0] * C4 + tmp2[1] * -C4;
	tmp[2] = tmp2[2] * C6 + tmp2[3] * C2;
	tmp[3] = tmp2[2] * -C2 + tmp2[3] * C6;
	tmp[4] = tmp2[4] + tmp2[5];
	tmp[5] = tmp2[4] - tmp2[5];
	tmp[6] = tmp2[7] - tmp2[6];
	tmp[7] = tmp2[6] + tmp2[7];
	

#ifdef TRACE
	printf("\n==== Stage 3 ====\n");
	for (i=0;i<8;i++) {
		printf("stage3[%d] = %f\n",i,tmp[i]);
	}
#endif

	// Etage 4 à compléter
	out[0] = tmp[0]/2;
	out[1] = tmp[4]*C7 + tmp[7]*C1;
	out[1] /= 2;
	out[2] = tmp[2]/2;	
	out[3] = tmp[5] * (-C5) + tmp[6] * C3;
	out[3] /= 2;
	out[4] = tmp[1]/2;
	out[5] = tmp[5] * C3 + tmp[6] * C5;
	out[5] /= 2;
	out[6] = tmp[3]/2;
	out[7] = tmp[7] * C7 + tmp[4] * (-C1); 
	out[7] /= 2;

#ifdef TRACE
	printf("\n==== Output ====\n");
	for (i=0;i<8;i++) {
		printf("output[%d] = %f\n",i,out[i]);
	}
#endif
}

/*
 * Pour l'implémentation en virgule fixe nous avons utilisé le format Q11_4.
 */
void fast_fixed_dct8(short *in, short *out) {
	short input[8], tmp[8], tmp2[8];
	int i;

	// Passage en Q11_4
	for (i = 0; i < 8; i++)
		input[i] = in[i] << 4;

#ifdef TRACE
	printf("\n==== Input ====\n");
	for (i=0;i<8;i++) {
		printf("input[%d] = %d\n",i,input[i] >> 4);
	}
#endif

	tmp[0] = input[0] + input[7];	
	tmp[1] = input[1] + input[6];
	tmp[2] = input[2] + input[5];
	tmp[3] = input[3] + input[4];
	tmp[4] = input[0] - input[7];
	tmp[5] = input[1] - input[6];
	tmp[6] = input[2] - input[5];
	tmp[7] = input[3] - input[4];

#ifdef TRACE
	printf("\n==== Stage 1 ====\n");
	for (i=0;i<8;i++) {
		printf("stage1[%d] = %d\n",i,tmp[i] >> 4);
	}
#endif

	tmp2[0] = tmp[0] + tmp[3];
	tmp2[1] = tmp[1] + tmp[2];
	tmp2[2] = tmp[1] - tmp[2];
	tmp2[3] = tmp[0] - tmp[3];
	tmp2[4] = tmp[4];
	// Q11_4 * Q0_15 => Q11_19 (+ signe)
	tmp2[5] = ((int) (tmp[6] - tmp[5]) * C4_f) >> 15;
	tmp2[6] = ((int) (tmp[5] + tmp[6]) * C4_f) >> 15;
	tmp2[7] = tmp[7];

#ifdef TRACE
	printf("\n==== Stage 2 ====\n");
	for (i=0;i<8;i++) {
		printf("stage2[%d] = %d\n",i,tmp2[i] >> 4);
	}
#endif

	tmp[0] = ((int) (tmp2[0] + tmp2[1]) * C4_f) >> 15;
	tmp[1] = ((int) (tmp2[0] - tmp2[1]) * C4_f) >> 15;
	tmp[2] = (((int) tmp2[2] * C6_f) >> 15) + (((int) tmp2[3] * C2_f) >> 15);
	tmp[3] = (((int) tmp2[2] * -C2_f) >> 15) + (((int) tmp2[3] * C6_f) >> 15);
	tmp[4] = tmp2[4] + tmp2[5];
	tmp[5] = tmp2[4] - tmp2[5];
	tmp[6] = tmp2[7] - tmp2[6];
	tmp[7] = tmp2[6] + tmp2[7];

#ifdef TRACE
	printf("\n==== Stage 3 ====\n");
	for (i=0;i<8;i++) {
		printf("stage3[%d] = %d\n",i,tmp[i] >> 4);
	}
#endif

	out[0] = tmp[0] / 2;
	out[1] = (((int) tmp[4] * C7_f) >> 15) + (((int) tmp[7] * C1_f) >> 15);
	out[1] /= 2;
	out[2] = tmp[2] / 2;
	out[3] = (((int) tmp[5] * -C5_f) >> 15) + (((int) tmp[6] * C3_f) >> 15);
	out[3] /= 2;
	out[4] = tmp[1] / 2;
	out[5] = (((int) tmp[5] * C3_f) >> 15) + (((int) tmp[6] * C5_f) >> 15);
	out[5] /= 2;
	out[6] = tmp[3] / 2;
	out[7] = (((int) tmp[7] * C7_f) >> 15) + (((int) tmp[4] * -C1_f) >> 15);
	out[7] /= 2;

	for (i = 0; i < 8; i++)
		out[i] = out[i] >> 4;

#ifdef TRACE
	printf("\n==== Output ====\n");
	for (i=0;i<8;i++) {
		printf("output[%d] = %d\n",i,out[i]);
	}
#endif
}

static void transpose_lignes_colonnes(short in[8][8], short out[8][8]) {
	int i, j;

	for (i = 0; i < 8; i++) {
		for (j = 0; j < 8; j++)
			out[j][i] = in[i][j];
	}
}

void fast_fixed_dct8x8(short pixel[8][8], short data[8][8])
{
	short ping[8][8], pong[8][8];
	int i, j;

	for (i = 0; i < 8; i++)
		fast_fixed_dct8(pixel[i], ping[i]);

	transpose_lignes_colonnes(ping, pong);

	for (i = 0; i < 8; i++)
		fast_fixed_dct8(pong[i], ping[i]);

	transpose_lignes_colonnes(ping, data);
}
